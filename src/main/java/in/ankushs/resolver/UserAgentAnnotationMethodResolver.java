package in.ankushs.resolver;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import in.ankushs.annotation.UserAgent;

public class UserAgentAnnotationMethodResolver implements HandlerMethodArgumentResolver{

	
	@Override
	public Object resolveArgument(
			final MethodParameter methodParam,
			final ModelAndViewContainer modelAndViewContainer, 
			final NativeWebRequest webRequest,
			final WebDataBinderFactory webDataBinderFactory) throws Exception 
	{
		String result = "";
		if(webRequest.getNativeRequest() instanceof HttpServletRequest){
			final HttpServletRequest httpServletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
			result = httpServletRequest.getHeader("User-Agent");
		}
		return result;
	}

	@Override
	public boolean supportsParameter(final MethodParameter methodParam) {
		return methodParam.hasParameterAnnotation(UserAgent.class);
	}

}

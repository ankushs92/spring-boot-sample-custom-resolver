package in.ankushs.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import in.ankushs.annotation.UserAgent;

@RestController
public class TestController {
	
	@GetMapping
	public String resolveUserAgent(@UserAgent String userAgent){
		return "userAgent is : " + userAgent;
	}
	
}

package in.ankushs.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import in.ankushs.resolver.UserAgentAnnotationMethodResolver;

@Configuration
@EnableWebMvc
public class BindersConfig extends WebMvcConfigurerAdapter{

	 @Override
     public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
         argumentResolvers.add( new UserAgentAnnotationMethodResolver());
     }
}


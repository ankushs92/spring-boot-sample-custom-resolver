package in.ankushs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSampleCustomResolversApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSampleCustomResolversApplication.class, args);
	}
}
